/**
 * Created by jorgeromanespino on 12/2/17.
 */
package main

import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup

//import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow

//
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.exceptions.TelegramApiException


// Resources
// https://irazasyed.github.io/telegram-bot-sdk/usage/keyboards/
// https://github.com/rubenlagus/TelegramBots/blob/master/telegrambots-meta/src/main/java/org/telegram/telegrambots/api/objects/replykeyboard/ReplyKeyboardMarkup.java
// https://core.telegram.org/bots
// https://core.telegram.org/bots/api/#replykeyboardmarkup

class MyTelegramBot extends TelegramLongPollingBot {
    @Override
    public String getBotUsername() {
        return "MyTelegramBot";
    }

    @Override
    public String getBotToken() {
        return "";
    }

    /*
    @Override
    public void onUpdateReceived(Update update) {
        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            SendMessage message = new SendMessage() // Create a SendMessage object with mandatory fields
                    .setChatId(update.getMessage().getChatId())
                    .setText(update.getMessage().getText());
            try {
                sendMessage(message); // Call method to send the message
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }
    */


    String broker = "tcp://192.168.0.100:1883"
    String topic = 'PlantaBaja/Entrada/InterruptorLuz'

    MQTTPublisher publisher = new MQTTPublisher(broker: broker, clientId: 'publisher1')

    static Map conversations = [:]

    def parseMessage(Message inputMessage) {
        // Create a SendMessage object with mandatory fields
        SendMessage outputMessage = new SendMessage()
        def chatId = inputMessage.chatId
        outputMessage.chatId = chatId
        String text = inputMessage.text.toLowerCase()
        if (text == "encender") {
            if (conversations[chatId] == "entrada") {
                outputMessage.text = "Encendiendo entrada"
                publisher.publish(topic, '1')
                conversations[chatId] = ""
            } else {
                outputMessage.text = "Escoge opción:"
                KeyboardRow row = new KeyboardRow()
                row.add("Entrada")
                row.add("...")
                ReplyKeyboardMarkup rkm = new ReplyKeyboardMarkup(
                        keyboard: [row],
                        resizeKeyboard: true,
                        oneTimeKeyboad: true,
                        selective: false
                )
                outputMessage.replyMarkup = rkm
                conversations[chatId] = text
            }
        } else if (text == "entrada") {
            if (conversations[chatId] == "encender") {
                outputMessage.text = "Encendiendo entrada"
                publisher.publish(topic, '1')
                conversations[chatId] = ""
            } else {
                conversations[chatId] = text
            }
        } else if (text == "?") {
            outputMessage.text = "Escoge opción:"
            KeyboardRow row = new KeyboardRow()
            row.add("Encender")
            row.add("Entrada")
            row.add("...")
            ReplyKeyboardMarkup rkm = new ReplyKeyboardMarkup(
                    keyboard: [row],
                    resizeKeyboard: true,
                    oneTimeKeyboad: true,
                    selective: false
            )
            outputMessage.replyMarkup = rkm
            conversations[chatId] = text
        }
        outputMessage
    }

    @Override
    public void onUpdateReceived(Update update) {
        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.message.hasText()) {
            def outputMessage = parseMessage(update.message)
            try {
                sendMessage(outputMessage); // Call method to send the message
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

}