package main
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.apache.commons.logging.LogFactory

/*
 * Created by jorgeromanespino on 12/2/17.
 */
class MQTTPublisher {

    protected final log = LogFactory.getLog(this.getClass())

    String broker
    String clientId     = "Publisher";
    MqttClientPersistence persistence = new MemoryPersistence()

    def publish(String topic, String content, int qos = 2) {
        log.debug "publish '$content'"
        try {
            MqttClient client = new MqttClient(broker, clientId, persistence)
            MqttConnectOptions connOpts = new MqttConnectOptions()
            connOpts.setCleanSession(true);
            log.trace "Connecting to broker: $broker"
            client.connect(connOpts)
            log.trace "Connected"
            log.trace "Publishing message: $content"
            MqttMessage message = new MqttMessage(content.getBytes())
            message.setQos(qos)
            client.publish(topic, message)
            log.trace "Message published"
            client.disconnect()
            log.trace "Disconnected"
            client.close()
            log.trace "closed"
            true
        } catch(MqttException me) {
            log.error(me)
            log.trace "reason ${me.getReasonCode()}"
            log.trace "msg ${me.getMessage()}"
            log.trace "loc ${me.getLocalizedMessage()}"
            log.trace "cause ${me.getCause()}"
            log.trace "excep ${me}"
            log.trace "me.printStackTrace()"
            throw me
        }
    }
}

