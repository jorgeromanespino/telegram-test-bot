package main

//
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.TelegramBotsApi
import org.telegram.telegrambots.exceptions.TelegramApiException

/**
 * Created by jorgeromanespino on 12/2/17.
 */
class Main {

    //
    static void main(args) {
        println "main.MainGroovy starting point"

        ApiContextInitializer.init();

        TelegramBotsApi botsApi = new TelegramBotsApi()

        try {
            botsApi.registerBot(new MyTelegramBot())
        } catch (TelegramApiException e) {
            e.printStackTrace()
        }

        println "main.MainGroovy ending point"
    }

}
